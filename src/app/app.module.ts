import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CryptoComponent } from './crypto/crypto.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { PokedexComponent } from './pokedex/pokedex.component';
import { MainComponent } from './main/main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule  } from "@angular/material/input";
import { HttpClientModule } from '@angular/common/http';
import {MatSelectModule} from '@angular/material/select';
import { cryptoService } from "./crypto/crypto.service";
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatPaginatorModule} from '@angular/material/paginator';
import { PokedexService } from './pokedex/pokedex.service';
import {MatChipsModule} from '@angular/material/chips';
import { DetalleComponent } from './pokedex/detalle/detalle.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {MatTabsModule} from '@angular/material/tabs';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {MatAutocompleteModule} from '@angular/material/autocomplete';



@NgModule({
  declarations: [    
    AppComponent,
    CryptoComponent,
    PokedexComponent,
    MainComponent,
    DetalleComponent
  ],
  imports: [
    FormsModule,
    MatTabsModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    ScrollingModule,
    MatDividerModule,
    MatListModule,
    NgxChartsModule,
    MatChipsModule,
    MatIconModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    FlexLayoutModule,
    MatCardModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [cryptoService,PokedexService],  
  bootstrap: [AppComponent]
})
export class AppModule { }
