import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MediaObserver,MediaChange } from "@angular/flex-layout";
import { Subscription } from 'rxjs';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit{
  title = 'frontend';

  //private mediaSub: Subscription;
  private mediaSub:Subscription = new Subscription;
  constructor(
    private observer: MediaObserver,
    private detector: ChangeDetectorRef,
  ){}
  ngOnInit(){
    this.mediaSub =  this.observer.media$.subscribe((change: MediaChange)=>{
      console.log(change.mqAlias);
      console.log(change);
    })
  }
  ngAfterViewInit(){

  }
}
