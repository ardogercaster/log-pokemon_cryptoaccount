import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//routing 
import { CryptoComponent } from "./crypto/crypto.component";
import { PokedexComponent } from "./pokedex/pokedex.component";
import { DetalleComponent } from "./pokedex/detalle/detalle.component";
import { MainComponent } from "./main/main.component";

const routes: Routes = [
  //{ path: '', redirectTo: '/main', pathMatch: 'full' },
  { path: "" , component: MainComponent },
  { path: "crypto" , component: CryptoComponent},
  { path: "pokedex" , component: PokedexComponent},
  { path: 'pokedex/:name',  component: DetalleComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
