// Interfaz general
export class actividad {
    nombre!: string;
    path! : string;
    img! : string;
}

//clase para Crypto
export class CryptoAccount {
    id : number;
    name : string; //Bitcoin,
    symbol :  string;//BTC,
    balance :  number;//0.43,
    rateInUSD :  number;//7186.02
    URL : string ;//https://www.bitcoin.com/

    constructor(id: number,name: string,symbol: string,balance: number,rateInUSD: number){
        this.id = id;
        this.name = name;
        this.symbol = symbol;
        this.balance = balance;
        this.rateInUSD = rateInUSD;
        this.URL = "https://www.cryptocompare.com/coins/"+this.symbol+"/overview/USD";
    }

    getName(){ 
        return  this.name;
    } 
    getSymbol(){ 
        return  this.symbol;
    } 
    getBalance(){
        return this.balance;
    }
    getURL(){
        return this.URL;
    }


}

