import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { CryptoAccount } from "../types"

@Injectable()
export class cryptoService {
  api = "http://localhost:1337";
  cryptoApi ="https://min-api.cryptocompare.com/data/price?";

  constructor(private http: HttpClient) { }

  getWallets(): Observable<CryptoAccount> {
    let url = this.api+"/default";
    return this.http.get<CryptoAccount>(url);
  }
  getPrice(money:string): Observable<any>{
    let url = this.cryptoApi+"fsym="+money+"&tsyms=USD";
    return this.http.get<any>(url);
  }
}

