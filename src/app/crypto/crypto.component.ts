import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { cryptoService } from "./crypto.service";
import { coins } from "./coins";
import { CryptoAccount } from "../types";

@Component({
  selector: 'app-crypto',
  templateUrl: './crypto.component.html',
  styleUrls: ['./crypto.component.css']
})
export class CryptoComponent implements OnInit {

  wallets:any = [];
  origen:string = "BTC";
  destino:string = "ETH";
  amount: number = 0;
  total:number = 0;
  constructor(    
    private api : cryptoService,
    private _snackBar: MatSnackBar
  ) {

    for(let index in coins ){
      let item = new CryptoAccount(parseInt(index),coins[index].name,coins[index].symbol,0,0)
      this.api.getPrice(coins[index].symbol).subscribe((value)=>{
        item.rateInUSD = parseFloat(value.USD);        
      });

      let storage = localStorage.getItem(coins[index].symbol);
      let balance:any = 0;
      if(storage == null){
        balance  = (Math.random() * (10 - 0.01) + 0.01).toFixed(2);      
        item.balance = parseFloat(balance);
        localStorage.setItem(coins[index].symbol, balance);
      }else{        
        balance = storage
        item.balance = parseFloat(balance);
      }
      this.wallets.push(item);      
    }
    console.log(this.wallets);

   }

  ngOnInit(): void {
    //this.getData();
    setTimeout(()=>{
      this.calculaTotal(this.wallets);
    },1500);
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 1500,
    });
  }

  getData() {
    this.api.getWallets().subscribe((items)=>{      
      this.wallets = items;      
      for(let index in this.wallets){        
        this.api.getPrice(this.wallets[index].symbol).subscribe((value)=>{
          this.wallets[index].rateInUSD = value.USD
        })
      }      
    })
  }

  calculaTotal(arreglo:any){
    this.total = 0;
    arreglo.find((item:any)=>{
      this.total += (item.balance * item.rateInUSD);
    });
    this.total = parseFloat(this.total.toFixed(2));
    //console.log(this.total);
  }

  validator(moneda:string){
    //console.log(moneda);
    let limite = this.wallets.find((item:any)=>{
      return item.symbol == moneda;
    });
    //console.log(limite);
    if(this.amount < 0){
      this.amount = 0;
      this.openSnackBar('No puede ser menor a 0','');
    }else{      
      if(this.amount > limite.balance){
        this.amount = limite.balance;
        this.openSnackBar('No puede ser mayor a '+limite.balance,'');
      }
    }    
  }

  transfer(origen:any,destino:any,amount:number){
    if(amount == 0){
      this.openSnackBar('No pueden realizarse transacciones con 0','');
    }else if(origen == destino){
      this.openSnackBar('No puede transferirse a la misma cuenta','');
    }else{
      origen = this.wallets.find((item:any)=>{
        return item.symbol == origen;
      });
      destino = this.wallets.find((item:any)=>{
        return item.symbol == destino;
      });
      origen.balance = parseFloat((origen.balance - amount).toFixed(2));
      localStorage.removeItem(origen.symbol);
      localStorage.setItem(origen.symbol, origen.getBalance().toString());
      const toDolars = origen.rateInUSD * amount ;
      const toCrypto =  toDolars / destino.rateInUSD;
      //console.log(toCrypto);
      destino.balance = parseFloat((destino.balance + toCrypto).toFixed(2));
      localStorage.removeItem(destino.symbol);
      localStorage.setItem(destino.symbol, destino.getBalance().toString());
      this.amount = 0;
      this.openSnackBar('Transaccion completada','');
    }  
  }  

}
