import { Component, OnInit } from '@angular/core';
import { pokelist } from "./pokelist";
import {PageEvent} from '@angular/material/paginator';
import { PokedexService } from "./pokedex.service";
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.css']
})
export class PokedexComponent implements OnInit {
  showItems: any;
  range = [-1,16];
  limite = 16;
  pokelist = pokelist;
  pokelistB: any = [];
  pageEvent!: PageEvent;
  postPerPage: any ;
  pageNumber : any;
  filteredOptions!: Observable<string[]>;
  myControl = new FormControl();
  constructor(private api : PokedexService, private router: Router,) {

    pokelist.find((item:any)=>{
      this.pokelistB.push(item.name);
    });
   
   }

   
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.pokelistB.filter((option: string) => option.toLowerCase().indexOf(filterValue) === 0);    
  }
  async ngOnInit() {    

    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );

    this.pokelist.sort((a, b) => a.name.localeCompare(b.name));
    this.showItems = pokelist.filter((item,$index)=>{      
      return $index < this.limite;
    })
    for(let index in this.showItems){
      await this.api.getPokemon(this.showItems[index].name).subscribe((res)=>{
        this.showItems[index].types = res.types;
      })
    }
  }
  goTo(event:any){
    console.log("this.myControl");
    console.log(event);
    this.router.navigate(['pokedex/'+event]);
    //console.log(this.myControl);
  }
  async pageChange(pageEvent: PageEvent) {
      this.range[0] = 0;     
      this.postPerPage = +pageEvent.pageSize;
      this.pageNumber = +pageEvent.pageIndex + 1;    
      pokelist.filter((item,$index)=>{    
        if($index < (this.limite * (this.pageNumber-1)))
          this.range[0] = $index+1;     
        else if($index < this.limite* this.pageNumber)
          this.range[1] = $index;                  
      });      
      this.showItems = pokelist.filter((item,$index)=>{
        return $index >= this.range[0] && $index <= this.range[1];
      });   
      for(let index in this.showItems){
        //console.log(typeof this.showItems[index].types)
        if(typeof this.showItems[index].types == 'undefined'){
          await this.api.getPokemon(this.showItems[index].name).subscribe((res)=>{
            this.showItems[index].types = res.types;
          })
        }
      }   
  }  
}
