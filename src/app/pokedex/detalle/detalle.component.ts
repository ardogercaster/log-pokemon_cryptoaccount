import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokedexService } from "../pokedex.service";
import * as shape from 'd3-shape';
//import { * as shape } from "../../../../node_modules/d3-shape";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { Subject } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css'],  
})
export class DetalleComponent implements OnInit {

  multi!: any;
  view: any = [0,0];  
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;  
  showMoves:any= [];
  range = [0,5];
  limite = 5;
  postPerPage : any;
  pageNumber : any;
  nameEvol: any;
  
  update$: Subject<any> = new Subject();

  //curve =  shape.curveLinearClosed;
  pokemon!:any;
  id!:any
  evolutions!:any;
  constructor(
    private api : PokedexService,
    private route: ActivatedRoute,    
  ) { 
    this.view = [(innerWidth / 1.3)/2, 300];
    Object.assign(this, { multi });
  }

  async ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('name');   
    this.loadData(this.id);
  }  
  changeData(id:any){
    this.id = id;
    this.loadData(this.id);
  }
  loadData(id:any){
    let stats:any = [];
    this.api.getSpecie(this.id).subscribe((res) => {
      this.api.getEvolution(res.evolution_chain.url).subscribe((res) => {
        this.evolutions = res;        
        //console.log(this.evolutions);
        let lista:any = [];
        this.getEvols(this.evolutions.chain,lista);
        this.nameEvol = lista;
        //console.log(lista);
        //console.log(this.nameEvol);
      })
    })
    this.api.getPokemon(this.id).subscribe((res) => {
      this.pokemon = res;
      //console.log(this.pokemon);
      this.pokemon.stats.find((item: any) => {
        stats.push({ name: item.stat.name.toUpperCase(), value: item.base_stat });
      });
      this.multi = { name: this.pokemon.name, series: stats };
      //console.log(this.multi);   
      this.multi = (JSON.parse(JSON.stringify(this.multi).replace(/^\{(.*)\}$/,"[ { $1 }]")));   
      this.updateChart();

      this.showMoves = this.pokemon.moves.filter((item: any,$index: number)=>{      
        return $index < this.limite;
      })      
    }) 
  }
  updateChart(){
    this.update$.next(true);
  }

  onResize(event:any) {
    this.view = [(event.target.innerWidth / 1.3)/2, 400];
  }
  colorScheme = {
    domain: ['#5AA454', '#E44D25']
  };
  async pageChange(pageEvent: PageEvent) {
    this.range[0] = 0;     
      this.postPerPage = +pageEvent.pageSize;
      this.pageNumber = +pageEvent.pageIndex + 1;    
      //showMoves
      this.pokemon.moves.filter((item: any,$index: number)=>{    
        if($index < (this.limite * (this.pageNumber-1)))
          this.range[0] = $index+1;     
        else if($index < this.limite* this.pageNumber)
          this.range[1] = $index;                  
      });   
      this.showMoves = this.pokemon.moves.filter((item: any,$index: number)=>{
        return $index >= this.range[0] && $index <= this.range[1];
      });    
    
  }
  onSelect(event:any) {
    //console.log(event);
  }
  getEvols(item:any,lista:any = false){        ;
    lista.push(item.species.name);   
    if(item.evolves_to.length > 0){
      this.getEvols(item.evolves_to[0],lista);
    }    
  }

}


var multi = [
  {
    "name": "MEW",
    "series": [
      {
        "name": "HP",
        "value": 0
      },
      {
        "name": "SPEED",
        "value": 0
      },
      {
        "name": "ATK",
        "value": 0
      },
      {
        "name": "DEF",
        "value": 0
      },
      {
        "name": "ATK ESP",
        "value": 0
      },
      {
        "name": "DEF ESP",
        "value": 0
      }
    ]
  },
];
