import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokedexService {

  api = " https://pokeapi.co/api/v2/"  
  constructor( private http: HttpClient ) {  }

  getPokemon(name:string): Observable<any> {
    const endpoint = "pokemon/"
    let url = this.api+endpoint+name;
    return this.http.get<any>(url);
  }

  getSpecie(name:string):Observable<any>{
    const endpoint ="pokemon-species/"
    let url = this.api+endpoint+name;
    return this.http.get<any>(url)
  }
  getEvolution(url:string):Observable<any>{
    return this.http.get<any>(url)
  }
}
