import { Component, OnInit } from '@angular/core';
import { actividad } from "../types";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  public actividades: actividad[] =  [
    
    { nombre: "CryptoFolio" ,  path:"crypto", img: "assets/crypto/btc.png"},    
    { nombre: "Pokedex"     ,  path:"pokedex", img : "assets/pokemon/logo.png"}
  ];

  
  constructor() { 
  }

  ngOnInit(): void {            
    this.makeActividades();
  }  

  public makeActividades(){
    console.log(this.actividades);
    //this.actividades.push();
  //  this.actividades.push();
  }

}
