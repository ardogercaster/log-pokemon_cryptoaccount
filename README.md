﻿# Pokemon & CryptoAccount 

Prueba desarrollada para  LOG (Logica de sistemas)
Se incorporaron ambos ejercicios al mismo proyecto, para aprovechar el framework.

## Pre-requisitos 📋

Angular 9+


## Testing 📦🛠️📦

* *FrontEnd*
	* npm install
	* ng serve
	* http://localhost:4200

## Autores ✒️
_Desarrolladores Involucrados_
* **Gerardo German Castañeda Hernandez** - [Ardoger](https://bitbucket.org/ardoger/) 
